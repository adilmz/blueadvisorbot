<?php

    $servername = "localhost";
    $username = "blueadvisor";
    $password = 'wV%$VyNyt(8%y4OIgg#6@)rRf99Gukg8';
    $dbname = "blueadvisor";

    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $verify_token = "BlueAdvisor"; // Verify token
    if (!empty($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe' && $_REQUEST['hub_verify_token'] == $verify_token) {
      echo $_REQUEST['hub_challenge'];
    }

    $input = json_decode(file_get_contents('php://input'), true);
    if ($input)
    {
      startBot($input, $conn);
    }

    function startBot($input, $conn)
    {
        $senderId 	 = $input['entry'][0]['messaging'][0]['sender']['id'];
        $messageText = $input['entry'][0]['messaging'][0]['message']['text'];
        $postback    = $input['entry'][0]['messaging'][0]['postback']['payload'];
        $coordinates = $input['entry'][0]['messaging'][0]['message']['attachments'][0]['payload']['coordinates'];

        //Delete User and Help
        if ($messageText == 'Exit' || $postback == 'FACEBOOK_WELCOME')
        {
          deleteFacebookUser($senderId, $conn);
          deleteHelps($senderId, $conn);
        }

        if(!empty($postback) || !empty($messageText))
        {

          $facebookUserSql = "SELECT facebookuser.*,TIMESTAMPDIFF(Minute,modifydate,now()) as `difference`  FROM facebookuser WHERE facebookuserid = $senderId and status = 1";
          $facebookUserQuery = mysqli_query($conn, $facebookUserSql);

          $InitMessage = true;
          $FacebookState = 1;
          $timediff = 0;

          if (mysqli_num_rows($facebookUserQuery) > 0)
          {
              if($facebookUser = mysqli_fetch_assoc($facebookUserQuery)) {
                $timediff = $facebookUser['difference'];
                if ($timediff < 5 && $messageText != 'Exit') {
                  $FacebookState = $facebookUser['chatstate'];
                  $InitMessage = false;
                }
              }
          }

          //Handle msg
          if ((!empty($messageText) && $messageText != 'Exit') || $postback == 'FACEBOOK_WELCOME')
          {
            newBotreply($messageText, $senderId, $conn);
            if ($messageText == 'ping') {
      				 $message['text'] = "Pong";
               sendMessage(['recipient' =>	['id' => $senderId ],'message'=> $message]);
      	    } elseif($messageText == 'Back'){
  			         firstStep($postback, $senderId, $timediff, $conn);
                 deleteHelps($senderId, $conn);
  		      } else {
            	if ($InitMessage) {
                  firstStep($messageText, $senderId, $timediff, $conn);
            	} else {
                  if ($FacebookState == 2) {
                    $InitMessage = false;
                    thirdStep($messageText, $senderId, $conn);
                  }

              		if($FacebookState == 3) {
                    $InitMessage = false;
                    fourStep($messageText, $senderId, $conn);
              		}
              }
      			}
          }

          //handle postback
          if(!empty($postback))
          {
            newBotreply($postback."----------POST", $senderId, $conn);

        		if($postback === 'BackCategory') {
        			firstStep($postback, $senderId, $timediff, $conn);
        		} else {
              $categoriesSql = "SELECT * FROM categories WHERE categoryname = '$postback' and status = 1 and deleted = '0'";
              $categoriesQuery = mysqli_query($conn, $categoriesSql);
              if (mysqli_num_rows($categoriesQuery) > 0) {
                if ($category = mysqli_fetch_array($categoriesQuery)) {
                  secondStep($category, $postback, $senderId, $conn);
                }
              }

              if ($FacebookState == 2) {
                $InitMessage = false;
                thirdStep($postback, $senderId, $conn);
              }
        		}
          }

        }

        //Location User
        if ($coordinates)
        {
          $userLat = $coordinates['lat'];
          $userLong = $coordinates['long'];
          $city = citylocation($userLat, $userLong);
			    newBotreply($city.'-------------CITY', $senderId, $conn);
          fourStep($city, $senderId, $conn);
        }

    }

    //Quesion STEPS
    function firstStep($messageText, $senderId, $timediff, $conn)
    {
        $categoriesSql = "SELECT * FROM categories WHERE parentcategory = 0 and status = 1 and deleted = '0'";
        $categoriesQuery = mysqli_query($conn, $categoriesSql);
        if (mysqli_num_rows($categoriesQuery) > 0) {
          $answer = "Welcome to BlueAdvisor!  Select a category. ";
          $message = sliderCategory($categoriesQuery, false);
          sendMessage(["recipient" =>	["id" => $senderId ],"message"=> ["text" => $answer]]);
          sendMessage(["recipient" =>	["id" => $senderId ],"message"=>  $message]);
        } else {
          sendMessage(["recipient" =>	["id" => $senderId ],"message"=> ["text" => "Welcome to BlueAdvisor!"]]);
        }

        if($timediff == 0 && $messageText != 'Exit') {
          deleteFacebookUser($senderId, $conn);
          newFacebookUser($senderId, $conn);
        } else {
          chatstateUpdate($senderId, 1, $conn);
        }
    }
    //
    function secondStep($category, $postback, $senderId, $conn)
    {
        if($category["categoryname"] == $postback && $category["parentcategory"] == 0)
        {
          $answer = "Pleaes select a sub-category. ";
		      sendMessage(['recipient' =>	['id' => $senderId ], 'message'=> ['text' => $answer]]);
          $catid = $category["id"];
          $parentcategoriesSql = "SELECT * FROM categories WHERE parentcategory = '$catid' and status = 1 and deleted='0'";
          $parentcategoriesQuery = mysqli_query($conn, $parentcategoriesSql);
          if (mysqli_num_rows($parentcategoriesQuery) > 0) {
            $message = sliderCategory($parentcategoriesQuery, true);
		         sendMessage(['recipient' =>	['id' => $senderId ], 'message'=> $message]);
          }
          chatstateUpdate($senderId, 2, $conn);

        }
    }

    function thirdStep($messageText, $senderId, $conn)
    {
        chatstateUpdate($senderId, 3, $conn);
		    newHelps($senderId, $conn);
        saveSubcategory($senderId, $messageText, $conn);
        sendMessage(['recipient' =>	['id' => $senderId ], 'message'=> ["text" => "Enter your city","quick_replies"=>[["content_type"=>"location"]]]]);
    }

    function fourStep($messageText, $senderId, $conn)
    {
        saveCity($messageText, $senderId, $conn);
        $helpSql = "SELECT * FROM helps WHERE url='$senderId' ORDER BY id DESC";
        $helpQuery = mysqli_query($conn, $helpSql);
        if (mysqli_num_rows($helpQuery) > 0) {
          if ($userHelper = mysqli_fetch_assoc($helpQuery)) {
            $sub = $userHelper['subcategory'];
            $city = $userHelper['city'];
            $adsSql = "SELECT * FROM ads WHERE subcategoryads='$sub' and cityads='$city'";
            $adsQuery = mysqli_query($conn, $adsSql);
            if (mysqli_num_rows($adsQuery) > 0) {
              chatstateUpdate($senderId, 4, $conn);
              $message = ["attachment" => ["type" => "template","payload" => ["template_type" => "generic","elements" => [["title" => "Thank You! An expert will contact you shortly. Please check your messages folder...","buttons" => [["type"=>"web_url","url"=>"google.com","title"=>"Terms and Conditions"]]]]]]];
              print_r($message);
              sendMessage(['recipient' =>	['id' => $senderId ],'message'=> $message]);
            } else {
              $answer = 'We are not available in your area.';
              sendMessage(['recipient' =>	['id' => $senderId ],'message'=> ['text' => $answer]]);
            }
          }
        }
    }

    //Helper Functions
    function sendMessage($response)
    {
         $accessToken = 'EAADSG4f752kBAL2MdyMtDPXNedFCMTwtY6DRdZCUaF8jZAPHelwAHU2A4CNKlJ3F6vZAUB4L0qc9ZC3kit5QXPDDW3RoMgN1DxYxRrdDY7JuUL60yPSmRZBVac9a17JZAQX71MHKrgttM5s9qhcjuZCVKZASfyOIkgjrmW7wvBLaLQZDZD';
        //$accessToken = 'EAAeVq9JD5XsBANxiGYpgRwtyQaRdpW8cGu89brjUQuE6UdcffvZCCotlJfkDu1QZAZCJWhvAMMCsJbodhFyZAARY9kOns9tLIMTzovFZAq75ygzJxXjBHKoEmMMqYcQmCxkrGI1vroWecfIkCNwbevN3mmx4axOABw9vGLSjeA8KyP46tILZCg';
  			$ch = curl_init('https://graph.facebook.com/v2.6/me/messages?access_token=' . $accessToken);
  			curl_setopt($ch, CURLOPT_POST, 1);
  			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
  			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
  			curl_exec($ch);
  			curl_close($ch);
    }

    function get_web_page($url)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_USERAGENT      => "test", // name of client
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content  = curl_exec($ch);

        curl_close($ch);

        return json_decode($content);
    }

    function citylocation($lat,$lng)
    {
      $ch = curl_init("http://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng");
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $output = curl_exec($ch);
      curl_close($ch);
      $content = json_decode($output);
      $result = '';
      $address_components = (array) $content->results[0]->address_components;
      foreach ($address_components as $key => $address) {
        if($address->types[0] == 'locality') {
          $result = $address['long_name'];
        }
      }
      return $result;
    }

  	function sendBackButton($senderId)
    {
    		$message = ["attachment" => ["type" => "template","payload" => ["template_type" => "generic","elements" => [["title" => "want to go back","buttons" => [["type"=>"postback","title"=>"Yes","payload"=>"BackCategory"]]]]]]];
        sendMessage(['recipient' =>	['id' => $senderId ],'message'=> $message]);
  	}

	  function sliderCategory($categoriesQuery, $sub)
    {
        $items = array();
		    $buttons = array();

        while($category = mysqli_fetch_array($categoriesQuery)) {
    			$buttons = array(
              array(
                "type" => "postback",
                "title" => $category['categoryname'],
                "payload" => $category['categoryname']
              )
          );

          $items[] = array(
            "title"=> $category['categoryname'],
            "image_url" => "https://api.blueadvisor.xyz/img/".$category['categoryimage'],
            "subtitle"=> "",
            "buttons" => $buttons
          );
          //   $message['quick_replies'][] = ['content_type' =>'text','title' => $category['categoryname'],'payload' => $category['categoryname']];
        }

        if($sub) {
          $output = ["attachment" => ["type"=>"template","payload" => ["template_type" => "generic", "elements" => $items ]],"quick_replies" => [["content_type"=>"text", "title"=>"Back", "payload"=> "BackCategory"]]];
        } else {
          $output = ["attachment" => ["type"=>"template","payload" => ["template_type" => "generic", "elements" => $items ]]];
        }
        return $output;
    }

    //DB SAVE Functions
    function saveSubcategory($senderId, $messageText, $conn)
    {
        $saveSubcategorySql = "UPDATE helps SET status='0', subcategory='$messageText' WHERE url='$senderId' and status='0'";
        mysqli_query($conn, $saveSubcategorySql);
    }

    function saveCity($messageText, $senderId, $conn)
    {
        $saveSubcategorySql = "UPDATE helps SET status='1', city='$messageText' WHERE url='$senderId' and status='0'";
        mysqli_query($conn, $saveSubcategorySql);
    }

    function newBotreply($messageText, $senderId, $conn)
    {
        $botReplySql = "INSERT INTO botreply (message, messagedate, facebookuserid) VALUES ('$messageText', now(), '$senderId')";
        mysqli_query($conn, $botReplySql);
    }

    function newFacebookUser($senderId, $conn)
    {
        $facebookUser = "INSERT INTO facebookuser (facebookuserid, facebookurl, chatstate, status, createdate, modifydate) VALUES ('$senderId', '$senderId', '0','1', now(), now())";
        mysqli_query($conn, $facebookUser);
    }

    function newHelps($senderId, $conn)
    {
        $helpsSql = "INSERT INTO helps (url, link, status) VALUES ('$senderId', '$senderId', '0')";
        mysqli_query($conn, $helpsSql);
    }

    function chatstateUpdate($senderId, $state, $conn)
    {
        $facebookUserSql = "UPDATE facebookuser SET status='1', chatstate=$state, modifydate = now() WHERE facebookuserid='$senderId'";
        mysqli_query($conn, $facebookUserSql);
    }

    function deleteFacebookUser($senderId, $conn)
    {
      $facebookUserDelete = "DELETE FROM facebookuser WHERE facebookuserid='$senderId'";
      mysqli_query($conn, $facebookUserDelete);
    }

    function deleteHelps($senderId, $conn)
    {
      $helpsDelete = "DELETE FROM helps WHERE facebookuserid='$senderId' ORDER BY id DESC LIMIT 1";
      mysqli_query($conn, $helpsDelete);
    }


?>
