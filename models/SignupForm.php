<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\UserProfile;
use app\models\UserCategory;

/**
 * Signup form
 */
class SignupForm extends Model
{

    public $email;
    public $password;
    public $category;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [['email','category'], 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['category', 'required']
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {

        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $userProfile = new UserProfile();

        $userProfile->user_category_id = $this->category;
        $userProfile->save();
        $user->email = $this->email;
        $user->generateVerificationKey();
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->link('profile', $userProfile);

        return $user->save() ? $user : null;;
    }

}