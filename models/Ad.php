<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ads".
 *
 * @property int $id
 * @property string $subcategoryads
 * @property string $cityads
 * @property string $urlads
 */
class Ad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subcategoryads', 'cityads', 'urlads'], 'required'],
            [['subcategoryads', 'cityads'], 'string', 'max' => 150],
            [['urlads'], 'string', 'max' => 350],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subcategoryads' => 'Subcategoryads',
            'cityads' => 'Cityads',
            'urlads' => 'Urlads',
        ];
    }
}
