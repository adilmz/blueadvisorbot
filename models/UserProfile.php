<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $id
 * @property int $user_category_id
 * @property string $device_token
 * @property string $created_at
 *
 * @property User $user
 * @property UserCategory $userCategory
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_category_id'], 'integer'],
            [['created_at'], 'safe'],
            [['device_token'], 'string', 'max' => 255],
            [['user_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserCategory::className(), 'targetAttribute' => ['user_category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_category_id' => 'User Category ID',
            'device_token' => 'Device Token',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCategory()
    {
        return $this->hasOne(UserCategory::className(), ['id' => 'user_category_id']);
    }
}
