<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "botreply".
 *
 * @property int $id
 * @property string $message
 * @property string $status
 * @property string $deleted
 */
class Botreply extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'botreply';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'status', 'deleted'], 'string'],
            [['facebookuserid', 'messagedate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Message',
            'facebookuserid' => 'Facebookuserid',
            'messagedate' => 'Messagedate',
            'status' => 'Status',
            'deleted' => 'Deleted',
        ];
    }
}
