<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "facebookuser".
 *
 * @property int $id
 * @property string $facebookuserid
 * @property int $chatstate
 * @property string $status
 * @property string $createdate
 * @property string $modifydate
 */
class Facebookuser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'facebookuser';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['chatstate'], 'integer'],
            [['status', 'facebookurl'], 'string'],
            [['createdate', 'modifydate'], 'safe'],
            [['facebookuserid'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'facebookuserid' => 'Facebookuserid',
            'chatstate' => 'Chatstate',
            'status' => 'Status',
            'createdate' => 'Createdate',
            'modifydate' => 'Modifydate',
        ];
    }
}
