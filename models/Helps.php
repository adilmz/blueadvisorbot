<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "helps".
 *
 * @property int $id
 * @property string $subcategory
 * @property string $city
 * @property string $url
 * @property string $link
 * @property string $status
 */
class Helps extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'helps';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link', 'status'], 'string'],
            [['subcategory', 'city'], 'string', 'max' => 150],
            [['url'], 'string', 'max' => 350],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subcategory' => 'Subcategory',
            'city' => 'City',
            'url' => 'Url',
            'link' => 'Link',
            'status' => 'Status',
        ];
    }
}
