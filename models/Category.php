<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $categoryname
 * @property int $parentcategory
 * @property string $createdate
 * @property string $modifydate
 * @property string $status
 * @property string $deleted
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parentcategory'], 'integer'],
            [['createdate', 'modifydate'], 'safe'],
            [['status', 'deleted'], 'string'],
            [['categoryname'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'categoryname' => 'Categoryname',
            'parentcategory' => 'Parentcategory',
            'createdate' => 'Createdate',
            'modifydate' => 'Modifydate',
            'status' => 'Status',
            'deleted' => 'Deleted',
        ];
    }
}
