<?php

namespace app\controllers;

use app\models\User;
use app\models\Facebookuser;
use app\models\Botreply;
use app\models\Category;
use app\models\Helps;
use app\models\Ad;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\base\Exception;

class BotController extends Controller
{
    private $accessToken = null;
    private $tokken = false;
    protected $client = null;

    public function beforeAction($action)
    {
        if ($action->id == 'index') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $accessToken = 'EAADSG4f752kBAL2MdyMtDPXNedFCMTwtY6DRdZCUaF8jZAPHelwAHU2A4CNKlJ3F6vZAUB4L0qc9ZC3kit5QXPDDW3RoMgN1DxYxRrdDY7JuUL60yPSmRZBVac9a17JZAQX71MHKrgttM5s9qhcjuZCVKZASfyOIkgjrmW7wvBLaLQZDZD';
        $this->setAccessToken($accessToken);
        $this->verifyTokken();
        $input = json_decode(file_get_contents('php://input'), true);
        if ($input) {
          $this->startBot($input);
        }
    }

    public function actionCat()
    {
        $categories = Category::find()->filterWhere(['parentcategory' => 0,'status' => '1','deleted' => '0'])->all();
        foreach ($categories as $key => $value) {
          echo "$value->categoryname<br>";
        }

    }

    public function setAccessToken($value)
    {
        $this->accessToken = $value;
    }

    public function startBot($input)
    {
        $senderId 	 = $input['entry'][0]['messaging'][0]['sender']['id'];
        $messageText = $input['entry'][0]['messaging'][0]['message']['text'];
        $postback    = $input['entry'][0]['messaging'][0]['postback']['payload'];
        $coordinates = $input['entry'][0]['messaging'][0]['message']['attachments'][0]['payload']['coordinates'];

        //Delete User and Help
        if ($messageText == 'Exit' || $postback == 'FACEBOOK_WELCOME')
        {
          $this->deleteFacebookUser($senderId);
          $this->deleteHelps($senderId);
        }

        if(!empty($postback) || !empty($messageText))
        {
          $facebookUser = Yii::$app->db
                    ->createCommand('SELECT facebookuser.*,TIMESTAMPDIFF(Minute,modifydate,now()) as `difference`  FROM facebookuser WHERE facebookuserid=:facebookuserid and status = 1')
                    ->bindParam(':facebookuserid', $senderId)->queryOne();
          $InitMessage = true;
          $FacebookState = 1;
          $timediff = 0;

          if ($facebookUser)
          {
            $timediff = $facebookUser['difference'];
            if ($timediff < 5 && $messageText != 'Exit') {
              $FacebookState = $facebookUser['chatstate'];
              $InitMessage = false;
            }
          }

          //Handle msg
          if ((!empty($messageText) && $messageText != 'Exit') || $postback == 'FACEBOOK_WELCOME')
          {
            $this->newBotreply($messageText, $senderId);
            if ($messageText == 'ping') {
      				 $message['text'] = "Pong";
               $this->sendMessage(['recipient' =>	['id' => $senderId ],'message'=> $message]);
      	    } elseif($messageText == 'Back'){
  			         $this->firstStep($postback, $senderId, $timediff);
                 $this->deleteHelps($senderId);
  		      } else {
            	if ($InitMessage) {
                  $this->firstStep($messageText, $senderId, $timediff);
            	} else {
                  if ($FacebookState == 2) {
                    $InitMessage = false;
                    $this->thirdStep($messageText, $senderId);
                  }

              		if($FacebookState == 3) {
                    $InitMessage = false;
                    $this->fourStep($messageText, $senderId);
              		}
              }
      			}
          }

          //handle postback
          if(!empty($postback))
          {
            $this->newBotreply($postback, $senderId);

        		if($postback === 'BackCategory') {
        			$this->firstStep($postback, $senderId, $timediff);
        		} else {
        			$category = Category::find()->filterWhere(['categoryname' => $postback,'status' => '1','deleted' => '0'])->one();
              if ($category) {
                $this->secondStep($category, $postback, $senderId);
              }
              if ($FacebookState == 2) {
                $InitMessage = false;
                $this->thirdStep($postback, $senderId);
              }
        		}
          }
        }

        //Location User
        if ($coordinates)
        {
          $this->newBotreply($coordinates, $senderId);
          $userLat = $coordinates['lat'];
          $userLong = $coordinates['long'];
          $city = $this->citylocation($userLat, $userLong);
          $this->fourStep($city, $senderId);
        }

    }

    //Quesion STEPS
    protected function firstStep($messageText, $senderId, $timediff)
    {
        $categories = Category::find()->filterWhere(['parentcategory' => 0,'status' => '1','deleted' => '0'])->all();
        $answer = "Welcome to BlueAdvisor!  Select a category. ";
        $message = $this->sliderCategory($categories, false);
        $this->sendMessage(["recipient" =>	["id" => $senderId ],"message"=> ["text" => $answer]]);
        $this->sendMessage(["recipient" =>	["id" => $senderId ],"message"=>  $message]);

        if($timediff == 0 && $messageText != 'Exit') {
          $this->deleteFacebookUser($senderId);
          $this->newFacebookUser($senderId);
        } else {
          $this->updateFacebookUser($senderId);
        }
    }

    protected function secondStep($category, $postback, $senderId)
    {
        if($category["categoryname"] == $postback && $category["parentcategory"] == 0) {
          $answer = "Pleaes select a sub-category. ";
          $InitMessage = false;
          $parentcategories = Category::find()->filterWhere(['parentcategory' => $category->id,'status' => '1','deleted' => '0'])->all();
          if ($parentcategories) {
            $message = $this->sliderCategory($parentcategories, true);
          }
          $this->chatstateUpdate($senderId, 2);

          $this->sendMessage(['recipient' =>	['id' => $senderId ], 'message'=> ['text' => $answer]]);
          $this->sendMessage(['recipient' =>	['id' => $senderId ], 'message'=> $message]);

        }
    }

    protected function thirdStep($messageText, $senderId)
    {
        $this->chatstateUpdate($senderId, 3);
		    $this->newHelps($senderId);
        $this->saveSubcategory($senderId, $messageText);
        $this->sendMessage(['recipient' =>	['id' => $senderId ], 'message'=> ["text" => "Enter your city","quick_replies"=>[["content_type"=>"location"]]]]);
    }

    protected function fourStep($messageText, $senderId)
    {
        $this->saveCity($senderId, $messageText);
        $userHelper = Helps::find()->where('url=:id', [':id' => $senderId])->orderBy(['id'=>SORT_DESC])->one();
        $ads = Ad::find()->andWhere(['subcategoryads' => $userHelper->subcategory])->andWhere(['cityads' => $userHelper->city])->one();
        if (count($ads) > 0) {
          $this->chatstateUpdate($senderId, 4);
          $message = ["attachment" => ["type" => "template","payload" => ["template_type" => "generic","elements" => [["title" => "Thank You! An expert will contact you shortly. Please check your messages folder...","buttons" => [["type"=>"web_url","url"=>"google.com","title"=>"Terms and Conditions"]]]]]]];
          $this->sendMessage(['recipient' =>	['id' => $senderId ],'message'=> $message]);
        } else {
          $answer = 'We are not available in your area.';
          $this->sendMessage(['recipient' =>	['id' => $senderId ],'message'=> ['text' => $answer]]);
        }

    }

    //Helper Functions
    protected function sendMessage($response)
    {
  			$ch = curl_init('https://graph.facebook.com/v2.6/me/messages?access_token=' . $this->accessToken);
  			curl_setopt($ch, CURLOPT_POST, 1);
  			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
  			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
  			curl_exec($ch);
  			curl_close($ch);
    }

    protected function verifyTokken()
    {
        try
        {
          $request = Yii::$app->request;
        	$hub_verify_token = $request->get('hub_verify_token');
          $local_hub = 'BlueAdvisor';

          if ($hub_verify_token === $local_hub) {
            echo $request->get('hub_challenge');
            exit;
          }
        }
        catch(Exception $ex)
        {
            // file_put_contents('../web/abc.txt', $ex->getMessage().' Verify');
        }
    }

    protected function get_web_page($url)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_USERAGENT      => "test", // name of client
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content  = curl_exec($ch);

        curl_close($ch);

        return json_decode($content);
    }

    protected function citylocation($lat,$lng)
    {
      $ch = curl_init("http://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng");
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $output = curl_exec($ch);
      curl_close($ch);
      $content = json_decode($output);
      $result = '';
      $address_components = (array) $content->results[0]->address_components;
      foreach ($address_components as $key => $address) {
        if($address->types[0] == 'locality') {
          $result = $address->long_name;
        }
      }
      return $result;
    }

  	protected function sendBackButton($senderId)
    {
    		$message = ["attachment" => ["type" => "template","payload" => ["template_type" => "generic","elements" => [["title" => "want to go back","buttons" => [["type"=>"postback","title"=>"Yes","payload"=>"BackCategory"]]]]]]];
        $this->sendMessage(['recipient' =>	['id' => $senderId ],'message'=> $message]);
  	}

	  protected function sliderCategory($categories, $sub)
    {
        $items = array();
		    $buttons = array();

        foreach ($categories as $key => $category) {
    			$buttons = array(
                array(
                  "type" => "postback",
                  "title" => $category->categoryname,
                  "payload" => $category->categoryname
                )
            );
            $items[] = array(
              "title"=> $category->categoryname,
              "image_url" => $category->categoryimage,
              "subtitle"=> "",
              "buttons" => $buttons
            );
            //   $message['quick_replies'][] = ['content_type' =>'text','title' => $category->categoryname,'payload' => $category->categoryname];
        }

          if($sub) {
           $output = ["attachment" => ["type"=>"template","payload" => ["template_type" => "generic", "elements" => $items ]],"quick_replies" => [["content_type"=>"text", "title"=>"Back", "payload"=> "BackCategory"]]];
          } else {
           $output = ["attachment" => ["type"=>"template","payload" => ["template_type" => "generic", "elements" => $items ]]];
          }

          return $output;
    }

    //DB SAVE Functions
    protected function saveSubcategory($senderId, $messageText)
    {
      $updatehelps = Helps::find()->filterWhere(['url' => $senderId, 'status' => '0'])->one();
      if ($updatehelps) {
        $updatehelps->subcategory = $messageText;
        $updatehelps->status = "0";
        $updatehelps->save();
      }
    }

    protected function saveCity($senderId, $messageText)
    {
      $updatehelps = Helps::find()->filterWhere(['url' => $senderId,'status' => '0'])->one();
      if ($updatehelps) {
        $updatehelps->city = $messageText;
        $updatehelps->status = "1";
        $updatehelps->save();
      }
    }

    protected function newBotreply($messageText, $senderId)
    {
      $facebookUser = new Botreply();
      $facebookUser->message = $messageText;
      $facebookUser->messagedate = date("Y-m-d H:i:s");
      $facebookUser->facebookuserid = $senderId;
      $facebookUser->save();
    }

    protected function newFacebookUser($senderId)
    {
      $facebookUser = new Facebookuser();
      $facebookUser->facebookuserid = $senderId;
      $facebookUser->status = "1";
      $facebookUser->chatstate = 1;
      $facebookUser->facebookurl = $senderId;
      $facebookUser->createdate = date("Y-m-d H:i:s");
      $facebookUser->modifydate = date("Y-m-d H:i:s");
      $facebookUser->save();
    }

    protected function newHelps($senderId)
    {
      $facebookUser = new Helps();
      $facebookUser->url = $senderId;
      $facebookUser->link = $senderId;
      $facebookUser->status = "0";
      $facebookUser->save();
    }

    protected function updateFacebookUser($senderId)
    {
      $facebookUser = Facebookuser::find()->where('facebookuserid=:id', array(':id'=>$senderId))->one();
      if($facebookUser) {
        $facebookUser->status = "1";
        $facebookUser->chatstate = 1;
        $facebookUser->facebookurl = $senderId;
        $facebookUser->modifydate = date("Y-m-d H:i:s");
        $facebookUser->save();
      }
    }

    protected function chatstateUpdate($senderId, $state)
    {
      $facebookUser = Facebookuser::find()->where('facebookuserid=:id', array(':id'=>$senderId))->one();
      if($facebookUser) {
        $facebookUser->status = "1";
        $facebookUser->chatstate = $state;
        $facebookUser->facebookurl = $senderId;
        $facebookUser->modifydate = date("Y-m-d H:i:s");
        $facebookUser->save();
      }
    }

    protected function deleteFacebookUser($senderId)
    {
      $facebook = Facebookuser::find()->where('facebookuserid=:id', array(':id'=>$senderId))->orderBy('id')->one();
      if ($facebook) {
        $facebook->delete();
      }
    }

    protected function deleteHelps($senderId)
    {
      $help = Helps::find()->filterWhere(['url' => $senderId, 'status' => '0'])->orderBy('id')->one();
      if ($help) {
        $help->delete();
      }
    }

}
