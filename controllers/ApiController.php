<?php
/**
 * Created by PhpStorm.
 * User: bullet
 * Date: 09.07.18
 * Time: 0:05
 */

namespace app\controllers;

use app\models\User;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use app\models\SignupForm;
use app\models\LoginForm;
use app\models\Helps;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class ApiController extends Controller
{

    public function actionSignup()
    {
        $result = ['result' => false,
            'message' => '',
            'data' => []
        ];
        $signupForm = new SignupForm();
        $signupForm->load(Yii::$app->request->get(), '');

        if (!$signupForm->validate()) {
            $errors = implode(' ', $signupForm->getFirstErrors());
            $result['message'] = $errors;
            return Json::encode($result);
        }
        if ($user = $signupForm->signup()) {
            $result['result'] = true;
            $result['message'] = 'Success signup';
            $result['data'] = ['id' => $user->primaryKey];
        } else {
            $result['message'] = 'Something wrong';
        }

        return Json::encode($result);
    }

    public function actionLogin()
    {
        $result = ['result' => false,
            'message' => '',
            'data' => []
        ];
        $loginForm = new LoginForm();
        $loginForm->load(Yii::$app->request->get(), '');

        if (!$loginForm->validate()) {
            $errors = implode(' ', $loginForm->getFirstErrors());
            $result['message'] = $errors;
        } else {
            $user = $loginForm->getUser();
            $result['result'] = true;
            $result['message'] = 'Success signup';
            $result['data'] = ['id' => $user->primaryKey,
                'category' => $user->profile->user_category_id];

        }
        return Json::encode($result);
    }

    public function actionApproveAccount()
    {
        $result = ['result' => false,
            'message' => ''];
        $get = Yii::$app->request->get();
        $userId = $get['id'];
        $verificationKey = $get['verificationKey'];
        $user = User::findOne(['id' => $userId]);

        if (isset($user)) {
            if ($user->verification_key == $verificationKey) {
                $user->status = User::STATUS_ACTIVE;
                if ($user->save()) {
                    $result['result'] = true;
                    $result['message'] = 'Account activated';
                } else {
                    $result['result'] = false;
                    $result['message'] = implode(' ', $user->getFirstErrors());
                }
            } else {
                $result['result'] = false;
                $result['message'] = 'Incorrect verification key';
            }
        } else {
            $result['result'] = false;
            $result['message'] = 'Cannot find user';
        }

        return Json::encode($result);
    }

    public function actionUserRequests()
    {
        $get = Yii::$app->request->get();
        $needRefresh = true;
        $result = ['result' => false,
            'message' => '',
            'data' => [],
            'needRefresh' => true,
            'totalCount' => 0
        ];
        $dataProvider = new ActiveDataProvider([
            'query' => Helps::find()
                ->where(['subcategory' => $get['category']])
                ->asArray(),

            'pagination' => [
                'pageSize' => 30,
            ],
        ]);

        if (isset($get['page'])) $dataProvider->pagination->page = $get['page'];
        $data = $dataProvider->getModels();
        $totalCount = $dataProvider->getTotalCount();
        if ($dataProvider->getCount() < 30) $needRefresh = false;

        if (!isset($data)) {
            $result['needRefresh'] = false;
        } else {
            $result['result'] = true;
            $result['data'] = $data;
            $result['needRefresh'] = $needRefresh;
            $result['totalCount'] = $totalCount;
        }

        return Json::encode($result);
    }

    public function actionUserRequestByIndex()
    {
        $get = Yii::$app->request->get();
        $user = User::find()->limit(1)->offset($get['id'])->asArray()->all();
        return Json::encode($user[0]);
    }

   
}