<?php
/**
 * Created by PhpStorm.
 * User: bullet
 * Date: 20.07.18
 * Time: 20:58
 */

namespace app\controllers;

use yii\web\Controller;
use Wrep\Notificato\Notificato;
use Wrep\Notificato\Apns\Certificate;

class NotificationController extends Controller
{

    public function actionSendNotification(){

        $sertificate = \Yii::$app->params['certificateSSlDevelop'];
        $passphrase = \Yii::$app->params['passphrase'];
        $notificato = new Notificato($sertificate, $passphrase, false, Certificate::ENDPOINT_ENV_SANDBOX);

        $message = $notificato->messageBuilder()
            ->setDeviceToken('c269c088c4ac1225b3a96e8b05bdb9cbb27ca4f82fd8b65d028251e33b82026f')
            ->setExpiresAt(new \DateTime('+1 hour'))
            ->setAlert('The numbers are 4, 8, 15, 16, 23 and 42', 'accept-button', 'launch-image')
            ->setSound('spookysound')
            ->setContentAvailable(false)
            ->build();


        // The message is ready, let's send it!
        //  Be aware that this method is blocking and on failure Notificato will retry if necessary
        $messageEnvelope = $notificato->send($message);

        // The returned envelope contains usefull information about how many retries where needed and if sending succeeded
        echo $messageEnvelope->getFinalStatusDescription();
    }

}