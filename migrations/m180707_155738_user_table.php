<?php

use yii\db\Migration;
use app\models\UserCategory;

/**
 * Class m180707_155738_user_table
 */
class m180707_155738_user_table extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'verification_key' => $this->string(6),
            'password_hash' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
        ], $tableOptions);

        $this->createTable('user_profile', [
            'id' => $this->primaryKey(),
            'user_category_id' => $this->integer(),
            'device_token' => $this->string(255),
            'created_at' => $this->dateTime()->notNull()->defaultValue(date('Y-m-d H:i:s'))
        ], $tableOptions);

        $this->createTable('user_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32)->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk-user-user_profile', 'user', 'id', 'user_profile', 'id');
        $this->addForeignKey('fk-user_profile-user_category', 'user_profile', 'user_category_id', 'user_category', 'id');



       $category = [ "Accounting","Advertising","Auditing","Beauty Trends","Biology",
                               "Buisness Law","Car Maintenance","Car Insurance","Cat Care","Chemistry","Childcare","Computer Security","Computer Science","Civil Litigation","Community Relationships","Consumer Electronics","Cooking","Civil Ligation Law","Credits Cards","Dieting",                  "Digital Marketing","Dog Care","Economics","Electrical","Employment Law","English","Everything iPhone", "Everything Samsung","Fair wages","Family Relationships","Garden Care","Graphic Design", "Health Insurance","Increase property Resale","Insurance Settlment","Immigration Law", "Investment Plan","Information Tech","Increasing Sales","Insurance Settlement","Interior Design","IRS","Kitchen Remodel",
                               "Macbook Computers","Management", "Managing Employees","Math","Mediation","Mens Health","Mortgage Loans","New Business Concept","New Car Search",
                               "New Parents","Nutrition","PC Computers","Personal Finance",  "Personel Health","Personal Injury","Physical Exercise","Physics","Plumbing","Preventive Health", "Quick Books","Real Estate","Relationships","Roofing","SEO","Taxes","Truck Maintenance",  "Used Car Search",  "Web Development","Web Hosting","Womens Health","Yoga"];
        foreach ($category as $item) {
            $userCategory = new UserCategory();
            $userCategory->name = $item;
            $userCategory->save();
    }
    }

    public function down()
    {
        $this->dropForeignKey('fk-user-user_profile', 'user');
        $this->dropForeignKey('fk-user_profile-user_category', 'user_profile');

        $this->dropTable('user');
        $this->dropTable('user_profile');
        $this->dropTable('user_category');
    }
}
