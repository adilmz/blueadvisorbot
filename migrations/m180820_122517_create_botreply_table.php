<?php

use yii\db\Migration;

/**
 * Handles the creation of table `botreply`.
 */
class m180820_122517_create_botreply_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('botreply', [
            'id' => $this->primaryKey(),
            'message' => $this->text(),
            'facebookuserid' => $this->string(190),
            'messagedate' => $this->dateTime(),
            'status' => 'ENUM("1", "0") NOT NULL DEFAULT "1"',
            'deleted' => 'ENUM("1", "0") NOT NULL DEFAULT "0"'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('botreply');
    }
}
