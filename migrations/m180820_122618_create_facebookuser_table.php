<?php

use yii\db\Migration;

/**
 * Handles the creation of table `facebookuser`.
 */
class m180820_122618_create_facebookuser_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('facebookuser', [
            'id' => $this->primaryKey(),
            'facebookuserid' => $this->string(),
            'facebookurl' => $this->string(190),
            'chatstate' => $this->integer(),
            'status' => 'ENUM("1", "0") NOT NULL DEFAULT "1"',
            'createdate' => $this->dateTime(),
            'modifydate' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('facebookuser');
    }
}
