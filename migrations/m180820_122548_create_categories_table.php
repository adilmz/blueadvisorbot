<?php

use yii\db\Migration;

/**
 * Handles the creation of table `categories`.
 */
class m180820_122548_create_categories_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'categoryname' => $this->string(),
            'parentcategory' => $this->integer(),
            'categoryimage' => $this->text(),
            'createdate' => $this->dateTime(),
            'modifydate' => $this->dateTime(),
            'status' => 'ENUM("1", "0") NOT NULL DEFAULT "1"',
            'deleted' => 'ENUM("1", "0") NOT NULL DEFAULT "0"'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('categories');
    }
}
