<?php

use yii\db\Migration;

/**
 * Handles the creation of table `helps`.
 */
class m180820_122639_create_helps_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('helps', [
            'id' => $this->primaryKey(),
            'subcategory' => $this->string(150),
            'city' => $this->string(150),
            'url' => $this->string(350),
            'link' => $this->string(350),
            'status' => 'ENUM("1", "0") NOT NULL DEFAULT "1"',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('helps');
    }
}
