<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ads`.
 */
class m180820_115033_create_ads_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('ads', [
            'id' => $this->primaryKey(),
            'subcategoryads' => $this->string(150)->notNull(),
            'cityads' => $this->string(150)->notNull(),
            'urlads' => $this->string(350)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ads');
    }
}
